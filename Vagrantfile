#
# LICENSE UPL 1.0
#
# Copyright (c) 2018, 2020 Oracle and/or its affiliates.
#
# Since: July, 2018
# Author: gerald.venzl@oracle.com
# Description: Creates an Oracle database Vagrant virtual machine.
#
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
#

# -*- mode: ruby -*-
# vi: set ft=ruby :

# https://oracle-base.com/articles/12c/weblogic-silent-installation-12c
# https://github.com/oracle/vagrant-projects
# https://github.com/weblogic-community/weblogic-vagrant/tree/master/wls12c-solaris-clustered/puppet


# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

# Box metadata location and box name
BOX_URL = "https://oracle.github.io/vagrant-projects/boxes"
BOX_NAME = "oraclelinux/7"

unless Vagrant.has_plugin?("vagrant-proxyconf")
  puts 'Installing vagrant-proxyconf Plugin...'
  system('vagrant plugin install vagrant-proxyconf')
end

# Define constants
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # Use vagrant-env plugin if available
  if Vagrant.has_plugin?("vagrant-env")
    config.env.load(".env.local", ".env") # enable the plugin
  end

  # VM name
  VM_NAME = default_s('VM_NAME', 'vagrant-oracle-spl')

  # Memory for the VM (in MB, 2300 MB is ~2.25 GB)
  VM_MEMORY = default_i('VM_MEMORY', 16384)

  # VM time zone
  # If not specified, will be set to match host time zone (if possible)
  VM_SYSTEM_TIMEZONE = default_s('VM_SYSTEM_TIMEZONE', host_tz)

  # Oracle base directory
  VM_ORACLE_BASE = default_s('VM_ORACLE_BASE', '/opt/oracle')

  # Oracle home directory
  VM_ORACLE_HOME = default_s('VM_ORACLE_HOME', '/opt/oracle/product/19c/dbhome_1')

  # Oracle SID
  VM_ORACLE_SID = default_s('VM_ORACLE_SID', 'ORCLCDB')

  # PDB name
  VM_ORACLE_PDB = default_s('VM_ORACLE_PDB', 'ORCLPDB1')

  # Database character set
  VM_ORACLE_CHARACTERSET = default_s('VM_ORACLE_CHARACTERSET', 'AL32UTF8')

  # Oracle Database edition
  # Valid values are 'EE' for Enterprise Edition or 'SE2' for Standard Edition 2
  VM_ORACLE_EDITION = default_s('VM_ORACLE_EDITION', 'EE')

  # Listener port
  VM_LISTENER_PORT = default_i('VM_LISTENER_PORT', 1521)

  # EM Express port
  VM_EM_EXPRESS_PORT = default_i('VM_EM_EXPRESS_PORT', 5500)

  # Oracle Database password for SYS, SYSTEM and PDBADMIN accounts
  # If left blank, the password will be generated automatically
  VM_ORACLE_PWD = default_s('VM_ORACLE_PWD', '')
end

# Convenience methods
def default_s(key, default)
  ENV[key] && ! ENV[key].empty? ? ENV[key] : default
end

def default_i(key, default)
  default_s(key, default).to_i
end

def host_tz
  # get host time zone for setting VM time zone
  # if host time zone isn't an integer hour offset from GMT, fall back to UTC
  offset_sec = Time.now.gmt_offset
  if (offset_sec % (60 * 60)) == 0
    offset_hr = ((offset_sec / 60) / 60)
    timezone_suffix = offset_hr >= 0 ? "-#{offset_hr.to_s}" : "+#{(-offset_hr).to_s}"
    'Etc/GMT' + timezone_suffix
  else
    'UTC'
  end
end

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # config.vm.box = BOX_NAME
  # config.vm.box_url = "#{BOX_URL}/#{BOX_NAME}.json"
  config.vm.box = "generic/ubuntu1804"
  config.vm.define VM_NAME
  config.vm.box_check_update = false
  
  config.vm.synced_folder ".", "/vagrant", nfs: false
  config.vm.synced_folder "sf_ici-psrm-configuration/", "/media/sf_ici-psrm-configuration", create: true
  config.vm.synced_folder "sf_ici-psrm/", "/media/sf_ici-psrm", create: true
  config.vm.synced_folder "sf_ici-cm/", "/media/sf_ici-cm", create: true
  config.vm.synced_folder "sf_SKMICI/", "/media/sf_SKMICI", create: true
  config.vm.synced_folder "sf_vb_shared/", "/media/sf_vb_shared", create: true

  # Provider-specific configuration -- VirtualBox
  config.vm.provider "virtualbox" do |v|
    v.memory = VM_MEMORY
    v.name = VM_NAME
	v.customize ["modifyvm", :id, "--cpus", 4]
  end
  
  config.vm.provider :libvirt do |v|
    v.memory = VM_MEMORY
  end

  # add proxy configuration from host env - optional
  if Vagrant.has_plugin?("vagrant-proxyconf")
    puts "getting Proxy Configuration from Host..."
    if ENV["http_proxy"]
      puts "http_proxy: " + ENV["http_proxy"]
      config.proxy.http     = ENV["http_proxy"]
    end
    if ENV["https_proxy"]
      puts "https_proxy: " + ENV["https_proxy"]
      config.proxy.https    = ENV["https_proxy"]
    end
    if ENV["no_proxy"]
      config.proxy.no_proxy = ENV["no_proxy"]
    end
  end

  # VM hostname
  config.vm.hostname = VM_NAME

  # Oracle port forwarding
  config.vm.network "forwarded_port", guest: VM_LISTENER_PORT, host: VM_LISTENER_PORT
  config.vm.network "forwarded_port", guest: VM_EM_EXPRESS_PORT, host: VM_EM_EXPRESS_PORT
  config.vm.network "forwarded_port", guest: 6005, host: 6005
  config.vm.network "private_network", ip: "11.11.11.11"
  
  config.vm.provision "shell" do |s|
    s.inline = <<-SHELL
    sudo apt-get -y install software-properties-common
    sudo add-apt-repository ppa:openjdk-r/ppa -y 
    sudo add-apt-repository ppa:ondrej/php -y
    sudo apt-get -y update 
    sudo rm /etc/localtime && sudo ln -s /usr/share/zoneinfo/UTC /etc/localtime

	sudo apt-get install -y yum nano htop wget openjdk-8-jdk
	
	sudo timedatectl set-timezone UTC

	sudo groupadd -g 6000 oinstall &>/dev/null
	sudo useradd -u 6600 -g oinstall oracle &>/dev/null
	sudo groupadd -g 6000 oinstall &>/dev/null
	sudo useradd -u 6600 -g oinstall oracle &>/dev/null

	mkdir -p /tmp
	chmod -R 777 /tmp
	mkdir -p /u01/software
	mkdir -p /u01/app/oracle/middleware
	mkdir -p /u01/app/oracle/config/domains
	mkdir -p /u01/app/oracle/config/applications
	chown -R oracle:oinstall /u01
	chmod -R 777 /u01/

	echo "127.0.0.1 localhost localhost.localdomain localhost4 localhost4.localdomain4 ol6.localdomain ol6" | sudo tee /etc/hosts -a
	
	cp /vagrant/ora-response/wls.rsp.tmpl /u01/software/wls.rsp
	cp /vagrant/ora-response/fmw_infr.rsp.tmpl /u01/software/fmw_infr.rsp
	cp /vagrant/ora-response/oraInst.loc.tmpl /u01/software/oraInst.loc
	cp /vagrant/fmw_14.1.1.0.0_wls.jar /u01/software/fmw_14.1.1.0.0_wls.jar &>/dev/null
	
	export MW_HOME=/u01/app/oracle/middleware
	export WLS_HOME=$MW_HOME/wlserver
	export WL_HOME=$WLS_HOME	
	
	# java -Xmx1024m -jar /u01/software/fmw_14.1.1.0.0_wls.jar -silent -responseFile /u01/software/wls.rsp -invPtrLoc /u01/software/oraInst.loc &>/dev/null
	# $JAVA_HOME/bin/java -Xmx1024m -jar /u01/software/fmw_12.2.1.0.0_infrastructure.jar -silent -responseFile /u01/software/fmw_infr.rsp -invPtrLoc /u01/software/oraInst.loc &>/dev/null
	# . $WLS_HOME/server/bin/setWLSEnv.sh
	# java weblogic.version
	
    SHELL
  end


  # Provision everything on the first run
  # config.vm.provision "shell", path: "scripts/install.sh", env:
  #   {
  #      "SYSTEM_TIMEZONE"     => VM_SYSTEM_TIMEZONE,
  #      "ORACLE_BASE"         => VM_ORACLE_BASE,
  #      "ORACLE_HOME"         => VM_ORACLE_HOME,
  #      "ORACLE_SID"          => VM_ORACLE_SID,
  #      "ORACLE_PDB"          => VM_ORACLE_PDB,
  #      "ORACLE_CHARACTERSET" => VM_ORACLE_CHARACTERSET,
  #      "ORACLE_EDITION"      => VM_ORACLE_EDITION,
  #      "LISTENER_PORT"       => VM_LISTENER_PORT,
  #      "EM_EXPRESS_PORT"     => VM_EM_EXPRESS_PORT,
  #      "ORACLE_PWD"          => VM_ORACLE_PWD
  #   }

end
